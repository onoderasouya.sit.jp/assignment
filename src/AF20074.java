import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AF20074 {
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton checkoutButton;
    private JPanel root;
    private JTextPane textPane1;
    private JTextPane OrderedItem;
    private JButton cancelButton;
    int sum = 0;

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            sum += price;
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + ". It will be served as soon as possible.");
            textPane1.setText("Total    " + sum + " yen");
            String currentText = OrderedItem.getText();
            OrderedItem.setText(currentText + food + "  " + price + "yen" + "\n");
        }
    }

    public AF20074() {
        button1.setIcon(new ImageIcon(
                this.getClass().getResource("Tempura.jpg")
        ));
        button2.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.jpg")
        ));
        button3.setIcon(new ImageIcon(
                this.getClass().getResource("sushi.jpg")
        ));
        button4.setIcon(new ImageIcon(
                this.getClass().getResource("Udon.jpg")
        ));
        button5.setIcon(new ImageIcon(
                this.getClass().getResource("Frenchfries.jpg")
        ));
        button6.setIcon(new ImageIcon(
                this.getClass().getResource("steak.jpg")
        ));
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 800);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 600);
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi", 1200);
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 500);
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French fries", 450);
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak", 1500);
            }
        });
        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation1 = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation1 == 0) {
                    int confirmation2 = JOptionPane.showConfirmDialog(null,
                            "Would you like to takeout?",
                            "Takeout confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (confirmation2 == 0) {
                        sum = sum + (sum * 8 / 100);
                    }
                    if (confirmation2 == 1) {
                        sum = sum + (sum * 1 / 10);
                    }
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + sum + "yen including tax.");
                    sum = 0;
                    textPane1.setText("Total    " + sum + " yen");
                    OrderedItem.setText("");
                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation1 = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel?",
                        "Cancel confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation1 == 0) {
                    sum = 0;
                    textPane1.setText("Total    " + sum + " yen");
                    OrderedItem.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("AF20074");
        frame.setContentPane(new AF20074().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
